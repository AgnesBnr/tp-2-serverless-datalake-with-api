# TODO : Create a s3 bucket with aws_s3_bucket

resource "aws_s3_bucket" "b" {
  bucket = "s3-job-offer-bucket-tp-2-esme"
  acl    = "private"

  tags = {
    Name        = "s3-job-offer-bucket-tp-2-esme"
    Environment = "dev"
  }

  force_destroy = true

}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object

resource "aws_s3_bucket_object" "object" {
  bucket = "s3-job-offer-bucket-tp-2-esme"
  key    = "job_offers/raw"
  source = "/dev/null"

  etag = filemd5(source)
}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "s3-job-offer-bucket-tp-2-esme"

  topic {
    topic_arn     = aws_sns_topic.topic.arn
    events        = ["s3:ObjectCreated:*"]
    filter_suffix = ".csv"
  }
}